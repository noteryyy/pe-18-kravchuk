const userYearOfBirth = Number (prompt ('Введите год рождения пользователя'));
const userMonthOfBirth = Number (prompt ('Введите  месяц рождения пользователя'));
const userDayOfBirth = Number (prompt ('Введите день рождения пользователя'));

const nowYearOfBirth = 2019;
const nowMonthOfBirth = 12;
const nowDayOfBirth = 23;

if (
    isNaN (userYearOfBirth) || userYearOfBirth === 0 ||
    isNaN (userMonthOfBirth) || userMonthOfBirth === 0 ||
    isNaN (userDayOfBirth) || userDayOfBirth === 0
) {
    alert ('Смотри что вводишь, !');
    throw new Error ('Ошибка ввода.');
}